initialState =  {
  counter:0, playlists:[]
};

inc = {type:'INC',payload:1};
dec = {type:'DEC',payload:1};

[inc,dec,inc,inc,dec].reduce( (state, action)=> {
 
  switch(action.type){
      case 'INC': return { ...state, counter: state.counter + action.payload };
      case 'DEC': return { ...state, counter: state.counter - action.payload };
      default:    return state
  }
},initialState)

/// =============
/// SubReducers / Nested Reducers
/// =============



initialState = {
  counter: 0,
  playlists: []
};

inc = { type: "INC", payload: 1 };
dec = { type: "DEC", payload: 1 };

const counterReducer = (state = 0, action) => {
  switch (action.type) {
    case "INC": return state + action.payload;
    case "DEC": return state - action.payload;
    default:    return state;
  }
};

[inc, dec, inc, inc, dec].reduce((state, action) => {
      return {...state, 
        counter: counterReducer(state.counter,action),
        playlists: counterReducer(state.playlists,action),
      };
}, initialState);
