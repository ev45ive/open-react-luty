import { createStore, Reducer, combineReducers } from "redux";
import counter from "./reducers/counter";
import playlists, { playlistsLoad } from "./reducers/playlists";

const reducer = combineReducers({
  counter: counter,
  playlists: playlists
});

export const store = createStore(reducer);

store.dispatch(
  playlistsLoad([
    {
      id: 123,
      name: "React Hits!",
      color: "#ff00ff",
      favorite: true
    },
    {
      id: 234,
      name: "React Top20",
      color: "#00ffff",
      favorite: true
    },
    {
      id: 345,
      name: "Best of React",
      color: "#ffff00",
      favorite: false
    }
  ])
);
