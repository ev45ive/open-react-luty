import * as React from "react";
import { Album } from "../../models/Album";

type P = {
  album: Album;
};

export const AlbumCard: React.FunctionComponent<P> = props => {
  return (
    <div className="card">

      <img src={props.album.images[0].url} className="card-img-top" />

      <div className="card-body">
        <h5 className="title">{props.album.name}</h5>
      </div>
      
    </div>
  );
};

export default AlbumCard;
