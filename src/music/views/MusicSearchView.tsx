import React from "react";
import SearchForm from "../components/SearchForm";
import SearchResults from "../components/SearchResults";
import { Album } from "../../models/Album";
import { musicSearch } from "../../services";

import "./MusicSearchView.css";
import { RouteComponentProps } from "react-router-dom";

type P = {} & RouteComponentProps;
type S = {
  query: string;
  nextQuery: string;
  results: Album[];
  message: string;
};

export default class MusicSearchiew extends React.Component<P, S> {
  state: S = {
    message: "",
    nextQuery: "",
    query: "",
    results: [
      // {
      //   id: "123",
      //   name: "Placki",
      //   artists: [],
      //   images: [
      //     {
      //       url: "https://www.placecage.com/c/200/300"
      //     }
      //   ]
      // }
    ]
  };

  static getDerivedStateFromProps(props: P) {
    return { nextQuery: new URLSearchParams(props.location.search).get("q") };
  }

  componentDidUpdate() {
    if (this.state.query !== this.state.nextQuery) {
      this.search(this.state.nextQuery);
    }
  }

  search = (query: string) => {
    this.props.history.push({
      pathname: "/search",
      search: "?q=" + query
    });

    this.setState({
      query: query,
      message: "",
      results: []
    });
    musicSearch
      .search(query)
      .then(albums =>
        this.setState({
          results: albums
        })
      )
      .catch(err =>
        this.setState({
          message: err.message
        })
      );
  };

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <SearchForm onSearch={this.search} query={this.state.query}/>
          </div>
        </div>

        <div className="row">
          <div className="col">
            {this.state.message}
            <SearchResults wyniki={this.state.results} />
          </div>
        </div>
      </div>
    );
  }
}
