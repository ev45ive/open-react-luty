import * as React from "react";
import SearchForm from "../components/SearchForm";
import SearchResults from "../components/SearchResults";
import { MusicSearchCtx } from "../../contexts";

import "./MusicSearchView.css";

interface P {}

const MusicSearchiewCtx: React.FunctionComponent<P> = React.memo(props => {
  return (
    <div>
      <div className="row">
        <div className="col">
          <MusicSearchCtx.Consumer>
            {ctx => <SearchForm onSearch={ctx.onSearch} />}
          </MusicSearchCtx.Consumer>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <MusicSearchCtx.Consumer>
            {ctx => (
              <>
                {ctx.message}
                <SearchResults wyniki={ctx.results} />
              </>
            )}
          </MusicSearchCtx.Consumer>
        </div>
      </div>
    </div>
  );
});

export default MusicSearchiewCtx;
