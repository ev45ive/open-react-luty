import * as React from "react";
import { MusicSearchCtx } from "../contexts";
import { Album } from "../models/Album";
import { musicSearch } from "../services";

type S = { query: string; results: Album[]; message: string };

export default class MusicSearchProvider extends React.Component<{}, S> {
  state: S = {
    message: "",
    query: "",
    results: []
  };

  onSearch = (query: string) => {
    this.setState({
      message: "",
      results: []
    });
    musicSearch
      .search(query)
      .then(albums =>
        this.setState({
          results: albums
        })
      )
      .catch(err =>
        this.setState({
          message: err.message
        })
      );
  };

  public render() {
    return (
      <>
        <MusicSearchCtx.Provider
          value={{
            ...this.state,
            onSearch: this.onSearch
          }}
        >
          {this.props.children}
        </MusicSearchCtx.Provider>
      </>
    );
  }
}
