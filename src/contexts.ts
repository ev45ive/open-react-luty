import React from "react";
import { Album } from "./models/Album";
import SearchForm from './music/components/SearchForm';

type C = {
  query: string;
  results: Album[];
  message: string;
  onSearch(query: string): void;
};

export const MusicSearchCtx = React.createContext<C>({
  message: "",
  query: "",
  results: [],
  onSearch() {
    throw "No Context Provider";
  }
});

// const Consumer = MusicSearchCtx.Consumer;

// export const WrapInMusicSearchCtx = (Comp: React.Component) => {
//   return (props: any) => (
//     <>
//       <Consumer>{ctx => <Comp></Comp>}</Consumer>
//     </>
//   );
// };

// const SearchWithCtx = WrapInMusicSearchCtx(SearchForm)

// <SearchWithCtx></SearchWithCtx>