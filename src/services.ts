import { SearchService } from "./music/SearchService";
import ClientOauth2 from "client-oauth2";

// https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow
// https://www.npmjs.com/package/client-oauth2
export const auth = new ClientOauth2({
  clientId: "70599ee5812a4a16abd861625a38f5a6",
  authorizationUri: "https://accounts.spotify.com/authorize",
  redirectUri: "http://localhost:3000/placki.html",
  // https://developer.spotify.com/documentation/general/guides/scopes/
  // scopes: ['notifications', 'gist']
  clientSecret: '624d68ed7e3d4db685957481bf8a2b1c',
  accessTokenUri: 'https://accounts.spotify.com/api/token',
});

const tokenJSON = sessionStorage.getItem("token");
if (tokenJSON) {
  const token = JSON.parse(tokenJSON);
  auth.createToken(token);
} else {
  window.addEventListener("message", e => {
    if (e.data && e.data.type === "Auth") {
      const s = new URLSearchParams(e.data.url);
      const token = auth.createToken({
        access_token: s.get("#access_token")!,
        expires_in: s.get("expires_in")!,
        token_type: s.get("token_type")!
      });
      // console.log(token);
      sessionStorage.setItem("token", JSON.stringify(token.data));
    }
  });
  // const redirectUri = auth.code.getUri({
  const redirectUri = auth.token.getUri({
    // query: { show_dialog: "true" }
  });
  // console.log(redirectUri)
  window.open(redirectUri, "_blank", "width=200");
}




export const musicSearch = new SearchService(
  "https://api.spotify.com/v1/search"
);
