import { Reducer, Action, ActionCreator } from "redux";

enum ActionTypes {
  Increment = "[Counter] Increment",
  Decrement = "[Counter] Decrement",
  Reset = "[Counter] Reset"
}
type S = number;
type Actions = Increment | Decrement | Reset;

const reducer: Reducer<number, Actions> = (state = 0, action) => {
  switch (action.type) {
    case ActionTypes.Increment:
      return state + action.payload;
    case ActionTypes.Decrement:
      return state - action.payload;
    case ActionTypes.Reset:
      return (state = action.payload);
    default:
      return state;
  }
};

export default reducer;

interface Increment extends Action<ActionTypes.Increment> {
  payload: number;
}

interface Decrement extends Action<ActionTypes.Decrement> {
  payload: number;
}

interface Reset extends Action<ActionTypes.Reset> {
  payload: number;
}

export const inc: ActionCreator<Increment> = (payload = 1) => ({
  type: ActionTypes.Increment,
  payload
});

export const dec: ActionCreator<Decrement> = (payload = 1) => ({
  type: ActionTypes.Decrement,
  payload
});

export const reset: ActionCreator<Reset> = (payload = 0) => ({
  type: ActionTypes.Reset,
  payload
});

export const counterActions = {
  inc,
  dec,
  reset
};

// console:
(window as any).counterActions = counterActions;

// store.subscribe(()=>console.log(store.getState()) )
// store.dispatch( counterActions.dec(5) )
