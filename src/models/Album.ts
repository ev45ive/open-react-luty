// https://developer.spotify.com/documentation/web-api/reference/search/search/

export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists: Artist[];
  images: AlbumImage[];
}

export interface Artist extends Entity {}

export interface AlbumImage {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
}

export interface SearchResponse {
  albums: PagingObject<Album>;
}
