import { connect } from "react-redux";
import { ItemsList } from "../components/ItemsList";
import { Playlist } from "../../models/Playlist";
import {
  PlaylistSelect,
  playlistsSelectorFrom,
  selectedPlaylistSelectorFrom,
  State
} from "../../reducers/playlists";

export const PlaylistsList = connect(
  (state: State) => ({
    playlists: playlistsSelectorFrom(state),
    selected: selectedPlaylistSelectorFrom(state)
  }),

  dispatch => ({
    onSelect: (selected: Playlist) => {
      dispatch(PlaylistSelect(selected.id));
    }
  })
)(ItemsList);
