import React from "react";
import { Playlist } from "../../models/Playlist";

type P = {
  playlists: Playlist[];
  onSelect(selected: Playlist): void;
  selected: Playlist | null;
};

type S = {};

export class ItemsList extends React.Component<P, S> {
  select(selected: Playlist) {
    this.props.onSelect(selected);
  }

  render() {
    return (
      <div>
        <div className="list-group">
          {this.props.playlists.map((playlist, index) => (
            <div
              className={
                "list-group-item" +
                (playlist === this.props.selected ? " active" : "")
              }
              key={playlist.id}
              onClick={() => this.select(playlist)}
            >
              {index + 1}. {playlist.name}
            </div>
          ))}
        </div>
      </div>
    );
  }
}
