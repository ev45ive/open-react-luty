import React from "react";
import { Playlist } from "../../models/Playlist";

type P = {
  playlist?: Playlist;
  onEdit(): void;
};

export const PlaylistDetails: React.FunctionComponent<P> = React.memo(
  ({ playlist, onEdit }) => {

    if (!playlist) {
      return <p>Select playlist </p>;
    }

    return (
      <div>
        <dl>
          <dt>Name:</dt>
          <dd>{playlist.name}</dd>

          <dt>Favourite:</dt>
          {playlist.favorite ? <span>Yes</span> : "No"}

          <dt>Color:</dt>
          <dd
            style={{
              color: playlist.color,
              backgroundColor: playlist.color
            }}
          >
            {playlist.color}
          </dd>
        </dl>

        <input
          type="button"
          value="Edit"
          className="btn btn-info"
          onClick={onEdit}
        />
      </div>
    );
  }
);

PlaylistDetails.defaultProps = {
  playlist: {
    id: 123,
    name: "React Hits!",
    color: "#ff00ff",
    favorite: true
  }
};

export default PlaylistDetails;
